const webpack = require('webpack');
const path = require('path');
const glob = require('glob');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const getHTMLWebpackPlugin = (filename, template, chunks) => {
  return new HtmlWebpackPlugin({
    filename,
    inject: 'body',
    chunks,
    template: 'nunjucks-html-loader!' + template
  });
}

function getHtmlWebpackPlugins(filesPath, sourcePath, chunks) {
  let entries = glob.sync(sourcePath + '*.njk');
  return entries.map(entry => {
    var fileName = entry.split('/').pop();
    return getHTMLWebpackPlugin( filesPath + fileName.replace('.njk', '/index.html'),
                                sourcePath + fileName,
                                chunks );
  });
}

module.exports = {
  mode: 'development',
  entry: {
    index: './src/js/index.js',
    channels: './src/js/channels.js',
    film: './src/js/film.js',
  },
  output: {
    path: path.resolve("build"),
    filename: 'js/[name]-[hash].js'
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
    minimizer: [
      new TerserJSPlugin({}),
      new OptimizeCSSAssetsPlugin({})
    ],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }, {
        test: /\.(scss|css)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '../',
            }
          },
          'css-loader', 'postcss-loader', 'sass-loader'
        ]
      }, {
        test: /\.(png|jpg|jpeg|gif|svg|pdf|mp4|webp)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              publicPath: '/',
              name: 'resources/[hash].[ext]',
            }
          }
        ]
      }, {
        test: /\.(woff|woff2|eot|ttf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              publicPath: '../fonts/',
              outputPath: 'fonts/'
            }
          }
        ]
      }, {
        test: /\.(html|njk|nunjucks)$/,
        use: [ {
          loader: 'html-loader',
          options: {
            attrs: [ 'img:src' ],
            interpolate: true
          }
        }, {
          loader:'nunjucks-html-loader',
          options : {
             searchPaths: [ './src/njk' ]
          }
        }]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/[name]-[contenthash].css',
      chunkFilename: 'css/[name]-[contenthash].css',
    }),
    getHTMLWebpackPlugin('index.html', './src/index.njk',
                        ['vendors~channels~film~index', 'vendors~index', 'index']),
    getHTMLWebpackPlugin('channels/index.html', './src/channels.njk',
                        ['vendors~channels~film~index', 'vendors~channels', 'channels']),
    ...getHtmlWebpackPlugins('films/', './src/films/',
                        ['vendors~channels~film~index', 'film']),
    new CopyWebpackPlugin([
      {
        context: path.resolve(__dirname),
        from: 'src/other/',
      }
    ]),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
      $: "jquery/dist/jquery",
      jQuery: "jquery/dist/jquery.min.js",
      "window.jQuery": "jquery/dist/jquery.min.js"
    }),
  ],
  resolve: {
    alias: {
      njk: path.join(__dirname, "src/njk"),
      img: path.resolve(__dirname, 'src/resources')
    },
    extensions: ['.Webpack.js', '.web.js', '.js']
  },
  devServer: {
    hot: true
  }
};
