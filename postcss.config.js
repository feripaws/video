module.exports = {
  plugins: {
    'autoprefixer': {
      grid: 'autoplace'
    },
  },
  options: {
    config: {
      ctx: {
        autoprefixer: {
          grid: 'autoplace'
        }
      }
    }
  }
}
