import {
  polyfillCustomEvent,
  showError
} from './helpers/';
import {
  getCookie,
  setCookie,
  deleteCookie,
} from './helpers/cookie-master';
import {
  getUser,
  setUser,
  updateUser
} from './helpers/ls-user';

polyfillCustomEvent();

$(function() {
  const $userPanel = $('#user-panel'),
        $loginPopup = $('#log-in-popup'),
        $username = $('#username'),
        $usernameInput = $('#username-input');

  var user = getUser(getCookie('user'));
  if (user) {
    window.userID = user.id;
    updateView();
  }

  function logIn(newName, save) {
    user = setUser(newName);
                                      // месяц
    setCookie('user', user.id, save ? 2592000000 : null);
    window.userID = user.id;
    updateView();
  }

  function logOut() {
    deleteCookie('user');
    user = null;
    window.userID = null;
    updateView();
  }

  function changeName(newName) {
    try {
      user = updateUser(user.id, newName);
      updateView();
    } catch(ex) { // ловим ошибку, что имя пользователя занято
      showError(ex);
    }
  }

  /*--- jquery ---*/
  $('.user').show(); // показываем контролы пользователя после
                     // того, как узнали, залогинен он или нет

  function updateView() {
    if (user && user.name) {
      $userPanel.addClass('user_logged');
      $username.text(user.name);
      $usernameInput.val(user.name);
    } else {
      $userPanel.removeClass('user_logged');
      $username.text('');
      $usernameInput.val('');
    }
    document.dispatchEvent(new CustomEvent('username-changed'));
  }

  $('#log-out').click(logOut);

  $('#log-in').submit(function(e) {
    e.preventDefault();
    // получаем введенные данные в виде объекта
    const serializedData = $(this).serializeArray();
    var formData = {};
    serializedData.forEach(function(field) {
      formData[field.name] = field.value;
    });

    logIn(formData.username, formData.save);

    $loginPopup.hide();
  })

  // включение формы входа
  $('#user-log-in').click(() =>
    $loginPopup.show()
               .find('input').first().focus() // автофокус на первом инпуте
  );

  // включение поля ввода нового имени
  $username.click(() => {
    $username.hide();
    $usernameInput.val(user.name).show().focus();
  })

  // пользователь ввел новое имя
  $usernameInput.blur(function() {
    var newName = $(this).val();
    $username.show();
    $usernameInput.hide();
    if (!newName || newName === user.name) { // если поле пустое - не изменяем
      return;                                // имя пользователя
    }

    changeName(newName);
  })
  /*---*/
});
