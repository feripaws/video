import "babel-polyfill";
import {
  getCommentsByFilmID,
  addComment as addCommentToLS,
  deleteComment
} from './helpers/ls-comments';
import { getUser } from './helpers/ls-user';
import { showError } from './helpers';

$(function() {
  var filmID = $('#film-id').text(),
      $comments = $('.comments__list');

  $('.comments').show();

  // подгружаем и показываем комментарии при загрузке страницы
  function loadComments() {
    getCommentsByFilmID(filmID)
      .forEach(comment =>
        addCommentCard( comment.id,
                        getUser(comment.userID),
                        comment.text )
      );
  }
  loadComments();

  function addComment(text) {
    const comment = addCommentToLS({
      userID: window.userID,
      filmID: filmID,
      text: text
    });
    addCommentCard(comment.id,
                   getUser(comment.userID),
                   comment.text);
  }

  /*--- jquery ---*/
  function addCommentCard(id, user, text) {
    const $card = $("<li>", {
      id,
      class: "comment"
    }).append(
      $("<h3>", {
        id: user.id,
        text: user.name,
        class: 'comment__user'
      })
    ).append(
      $("<p>", {
        text,
        class: "comment__text"
      })
    );

    if (user.id === window.userID) {
      $card.append(createDeleteBtn($card));
    }

    $comments.prepend($card);
  }

  function createDeleteBtn($card) {
    return $("<button>", {
        type: "button",
        class: "comment__delete",
        "aria-label": "Удалить комментарий"
      }).click(() => {
        try {
          if (deleteComment($card.attr('id'), window.userID)) {
            $card.remove();
          }
        } catch(ex) { // ловим запрет на удаление комментария
          showError(ex);
        }
      });
  }

  $('#new-comment').submit(function(e) {
    e.preventDefault();
    if (!window.userID) {
      showError('Чтобы оставлять комментарии, нужно зарегистрироваться на сайте');
      return;
    }

    const serializedData = $(this).serializeArray();
    var text = serializedData[0] ? serializedData[0].value : null;
    if (text) {
      addComment(text);
    }

    this.reset();
  })

  // обновляем комментарии (ники и возможность удаления)
  function updateUsernames() {
    $('.comment').each((index, commentElem) => {
      const $commentElem = $(commentElem),
            $username = $(commentElem).find('.comment__user'),
            curUser = getUser($username.attr('id')),
            $closeBtn = $commentElem.find('.comment__delete');
      // обновляем имя
      if (curUser.name && curUser.name !== $username.text()) {
        $username.text(curUser.name);
      }
      // добавляем / убираем кнопку удаления комментария
      if (window.userID === curUser.id && !$closeBtn.length) {
        $commentElem.append(createDeleteBtn($commentElem));
      } else if (window.userID !== curUser.id && $closeBtn.length) {
        $closeBtn.remove();
      }

    })
  }
  document.addEventListener('username-changed', updateUsernames, false);
  /*---*/
});
