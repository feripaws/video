import './common';
import '../scss/channels.scss';
import 'jquery.scrollbar';
import './helpers/modernizr-cssscrollbar';

$(function() {
  if (!Modernizr.cssscrollbar) {
    $('.scrollbar-inner').scrollbar();
  }

  const $window = $(window),
        $channels = $('.channels'),
        channelsTrueHeight = $channels[0].scrollHeight,
        emptyDocHeight = $(document).height() - $channels.height();

  // вмещаем список каналов вертикально в экран
  function resizeChannels() {
    var windowHeight = $window.height(),
        newHeight = windowHeight - emptyDocHeight;

    // только если экран не слишком маленький
    $channels.height(newHeight > 300 ? newHeight : channelsTrueHeight);
  }
  resizeChannels();

  $window.resize(resizeChannels);
});
