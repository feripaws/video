import './common';
import '../scss/index.scss';
import 'jquery-ui/ui/effects/effect-slide';

$(function() {
  /*--- поиск фильмов, содержащих запрошенный текст в названиях---*/
  var href = window.location.href;
  if (~href.indexOf('search=')) {
    var query = decodeURIComponent(
      href.split('search=')[1] // получаем строку между "search="
          .split(';')[0]       // и ";"
    );

    $('.gallery__element').has('.film-card__title')
      .filter(function() { // ищем вхождения запроса без учета регистра
        var filmTitle = $(this).find('.film-card__title').text();
        return !filmTitle.toLowerCase().includes(query.toLowerCase());
      })
      .hide(); // показываем только подошедшие фильмы
  }
  /*---*/

  /*--- слайдер ---*/
  var isScrolling = false;
  const initiateSlide = function(startScrollX, startMouseX, $gallery) {
    // начальные значения (пользователь нажал на слайдер)
    const galleryWidth = $gallery.width(),
          galleryFullWidth = $gallery.scrollWidth;
    var curScrollX = startScrollX,
        prevScrollX = startScrollX;

    $gallery
      .stop() // останавливаем все активные анимации
      .click(function(e) { // предотвращаем click во время прокрутки
        if (isScrolling) e.preventDefault();
      });

    return function(e, done) {
      // окончание (пользователь отпустил курсор)
      if (done) {
        var scrollSpeed = curScrollX - prevScrollX;
                // инерция после прокрутки
        $gallery.animate({ scrollLeft: curScrollX + scrollSpeed * 15 }, 800, 'easeOutCubic')
                // окончание прокрутки, больше не отслеживаем курсор
                .off('mousemove', this);
        setTimeout(() => isScrolling = false, 0);
        return;
      }

      // анимация слайдера (пользователь двигает курсор)
      var curMouseX = e.pageX - startMouseX;
      prevScrollX = curScrollX;
      curScrollX = startScrollX - curMouseX;

      // если движение слишком короткое, не считаем его за прокрутку
      if (!isScrolling && Math.abs(curMouseX) > 10) {
        isScrolling = true;
      }
      $gallery.scrollLeft(curScrollX);
    }
  }

  const $document = $(document),
        finishSlide = function(e, slideFunc) {
    slideFunc(e, true);
    $document.off('mousemove', slideFunc)
             .off('mouseup', this);
  };

  $('.gallery__list_films').mousedown(function(e) {
    e.preventDefault();

    const $this = $(this),
          slideFunc = initiateSlide($this.scrollLeft(), e.pageX, $this);
    $document.mousemove(slideFunc)
             .mouseup(e => finishSlide(e, slideFunc));
  })
  /*---*/
});
