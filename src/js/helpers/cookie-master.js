import "babel-polyfill";

const getCookie = function(key) {
  const cookies = document.cookie,
        encodedKey = encodeURIComponent(key);
  if (!cookies) return;

  var cookie = cookies
    .split(';') // разбиваем на отдельные куки
    .find(curCookie => // находим нужную куки по ключу
      curCookie.trim()
               .startsWith(encodedKey + '=')
    );

  if (!cookie) return;

  return decodeURIComponent(
    cookie.split('=')[1] // получаем значение
          .trim()
  );
};

const setCookie = function(key, value, deleteTime) {
  var expiration = '';
  if (deleteTime || deleteTime === 0) {
    let date = new Date();
    date.setTime(date.getTime() + deleteTime);
    expiration = date.toGMTString();
  }

  document.cookie = encodeURIComponent(key) + '=' + encodeURIComponent(value)
                    + ';path=/;expires=' + expiration;
}

const deleteCookie = key => setCookie(key, "", -1);

export {
  getCookie,
  setCookie,
  deleteCookie
}
