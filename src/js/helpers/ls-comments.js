import "babel-polyfill";
import shortid from "shortid";

const getComments = () => JSON.parse(localStorage.getItem('comments')) || [];
const setComments = users => localStorage.setItem('comments', JSON.stringify(users));

const areCommentsSet = () => !!getComments().length;

const getCommentsByFilmID = id =>
  getComments().filter(comment => comment.filmID === id);

const addComment = comment => {
  comment.id = comment.id || shortid.generate();
  var comments = getComments();
  comments.push(comment);
  setComments(comments);
  return comment;
}

const deleteComment = function(id, userID) {
  var comments = getComments()
    .filter(comment => {
      var isDeleted = comment.id === id;
      if (isDeleted && userID !== comment.userID) { // пользователь - не автор комментария
        throw 'Вы не можете удалять чужие комментарии';
      }
      return !isDeleted;
    });
  setComments(comments);
  return true;
}

export {
  areCommentsSet,
  getCommentsByFilmID,
  addComment,
  deleteComment
}
