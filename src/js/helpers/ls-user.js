import "babel-polyfill";
import shortid from "shortid";

const getUsers = () => JSON.parse(localStorage.getItem('users')) || {};
const setUsers = users => localStorage.setItem('users', JSON.stringify(users));

const getUserByName = function(name, users) {
  var usersList = users || getUsers(),
      userEntry = Object.entries(usersList)
                        .find(user => name === user[1]);

  if (!userEntry) return;

  return {
    id: userEntry[0],
    name: userEntry[1]
  };
}

const getUser = function(id) {
  var usersList = getUsers();
  if (!usersList[id]) return;

  return {
    id,
    name: usersList[id]
  };
}

const setUser = function(name) {
  var usersList = getUsers(),
      newUser = getUserByName(name, usersList);

  // если пользователя с таким именем еще нет - создаем
  if (!newUser) {
    newUser = {
      id: shortid.generate(),
      name
    };

    usersList[newUser.id] = newUser.name;
    setUsers(usersList);
  }

  return newUser;
}

const updateUser = function(id, name) {
  var usersList = getUsers();
  if (getUserByName(name, usersList)) {
    throw 'Пользователь с таким именем уже существует';
  }

  var newUser = {
    id,
    name
  };
  usersList[newUser.id] = newUser.name;
  setUsers(usersList);

  return newUser;
}

export {
  getUser,
  getUserByName,
  setUser,
  updateUser
}
