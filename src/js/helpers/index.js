// custom event polyfill for IE
// https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
const polyfillCustomEvent = function () {
  if ( typeof window.CustomEvent === "function" ) return false;

  function CustomEvent ( event, params ) {
    params = params || { bubbles: false, cancelable: false, detail: null };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
   }

  window.CustomEvent = CustomEvent;
};

// всплывающее окно об ошибках
var showError = message => alert(message);
$(function() {
  const $errorPopup = $('.popup_error');

  showError = function(message) {
    $errorPopup.find('.popup__message')
               .text(message);
    $errorPopup.show();
  }
  $errorPopup.find('button')
             .click(() => $errorPopup.hide()
                            .find('.popup__message').text(''));
});

export {
  polyfillCustomEvent,
  showError,
}
