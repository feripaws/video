import autosize from 'autosize';

import '../scss/common.scss';
import './login';
import {
  areCommentsSet,
  addComment,
} from './helpers/ls-comments';
import { setUser } from './helpers/ls-user';

import { defaultComments } from './default-data';

// комментарии и пользователи по умолчанию. загружаются, если информации
// о них нет в браузере
if (!areCommentsSet()) {
  defaultComments.forEach(comment => {
    var user = setUser(comment.user);
    delete comment.user;
    comment.userID = user.id;
    addComment(comment);
  });
}

$(function() {
  // textarea
  autosize($('.new-comment__input'));

  $('.searchbar').show();

  // попапы
  const $popups = $('.popup');
  $popups.find('.popup__background')
         .click(() => $popups.hide());

  // поиск
  $('.searchbar').submit(function(e) {
    e.preventDefault();

    const query = $(this).serializeArray()[0].value; // получаем искомый текст
    window.location.href = '/?search=' + encodeURIComponent(query);
  });
});
